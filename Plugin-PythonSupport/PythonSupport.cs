﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using VkNet.Model.RequestParams;
using VkBotClasses;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using System.Reflection;

namespace Plugin_PythonSupport
{
  

    public class PythonSupport
    {
        public static ScriptEngine engine = Python.CreateEngine();
        public static ScriptScope scope = engine.CreateScope();
        [BotCmd("python_test")]
        public static void PythonTest(VkBotRequest request) {
            
            scope.RemoveVariable("api");
            scope.RemoveVariable("");
            scope.SetVariable("api", request.ReceivedContextApi);
            scope.SetVariable("request_only_api", request.ApiRequestContext);
            scope.Engine.Execute("print api.isAuthorized()");

        }

        static void ClrAddReference(Assembly assembly)
        {
            engine.Runtime.LoadAssembly(assembly);
        }





    }
}

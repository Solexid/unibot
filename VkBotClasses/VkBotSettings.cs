﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VkBotClasses
{
    public class VkBotSettings
    {
            public string Token;
            public string BotNick;
            public string FriendlyBotName;
            public int MinDelay;
            public int MaxDelay;
            public long OwnerId;
            public bool Enabled;
            public bool SilentRequestsOnly;
            public string PluginsBlackList;
            public string CommandsBlackList;


    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace VkBotClasses
{
   
    public class VkBotRequest
    {
        public VkBotSettings BotSettings;
        public List<VkChat> Chats;
        public VkNet.VkApi ReceivedContextApi;
        public VkNet.VkApi ApiRequestContext;
        public  string cmd;
        public long ChatID;
        public string Message;
        public long MessageID;
        public long UserID;
        public bool isChat;
        public VkNet.Model.Attachments.Attachment[] Attachments;
        public VkNet.Model.Message[] Forwarded;
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VkBotClasses
{
   public class BotCmd : Attribute
    {
        public string names;
        public string info;
        public BotCmd()
        {
            this.names = "";
            this.info = "";
        }
        public BotCmd(string v)
        {
            this.names = v;
            this.info = "Нет описания.";
        }
        public BotCmd(string v,string info)
        {
            this.names = v;
            this.info = info;
        }
        public override bool Equals(object obj)
        {
            if (obj is string)
            {
                var split = names.Split(',');
                return split.Contains(((string)obj).Trim());
            }
            else
                return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), names);
        }
    }
}

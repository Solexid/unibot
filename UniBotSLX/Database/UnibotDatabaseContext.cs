﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using Microsoft.EntityFrameworkCore.Sqlite;
using Microsoft.EntityFrameworkCore;
namespace UniBotSLX.Database
{
    class UnibotDatabaseContext : DbContext
    {
        public static UnibotDatabaseContext _context;
        static string baseName = "unibot_db.db3";

        DbSet<Models.User> Users { get; set; }
        public UnibotDatabaseContext()
        {
       

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source="+ baseName);
        }

        public static UnibotDatabaseContext GetConnection()
        {

            return new UnibotDatabaseContext();
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniBotSLX.Database
{

    public class DBTableAttribute: Attribute
    {
        private string v;
        public DBTableAttribute()
        {
            this.v = "";
        }
        public DBTableAttribute(string v)
        {
            this.v = v;
        }
        public override bool Equals(object obj)
        {
            if (obj is string)
            {
                var split = v.Split(',');
                return split.Contains(((string)obj).Trim());
            }
            else
                return base.Equals(obj);
        }

    }

}

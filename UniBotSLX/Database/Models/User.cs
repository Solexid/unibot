﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
namespace UniBotSLX.Database.Models
{
    
    class User
    {
        public int Id { get; set; }
        public long VkBotUserID { get; set; } = -1;
        public long VkUserID { get; set; } = -1;
        public long TgBotUserID { get; set; } = -1;
        public long TgUserID { get; set; } = -1;
        public string JabberID { get; set; } = "";
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ulong MessagesCount { get; set; }
        public string JsonData { get; set; } = "";
        public DateTime Created { get; set; }
    }
}

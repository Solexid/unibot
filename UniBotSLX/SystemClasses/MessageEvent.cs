﻿using System;
using System.Collections.Generic;
using System.Text;
using VkBotClasses;

namespace UniBotSLX.EventSystem
{

    public enum BotMessageType
    {VK=1,
    Telegramm

    }
    class MessageEvent : EventArgs
    {
        public string MessageText  { get; set; }
        public VkBotRequest VkRequest{ get; set; }
        public BotMessageType MsgType { get; set; }
        // Constructor.
        public MessageEvent()
        {
           
        }

        
    }
    class EventSystem {

        public delegate void MessageventHandler(object sender, MessageEvent e);
        public static event MessageventHandler BotMessage;
     
        public static void OnMessageReceived(object instance,MessageEvent e)
        {
            BotMessage?.Invoke(instance, e);
        }

    }
}

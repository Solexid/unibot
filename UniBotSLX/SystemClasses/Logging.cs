using ConsoleSystemSLX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UniBotSLX
{
    public enum LogLevel
    {
        NOTHING = -1,
        ERRORS = 0,
        WARNINGS,
        VERBOSE,
        FULLDEBUG

    }


    class Logging
    {
        [ConsoleSystemSLX.ConsoleVariable("log_level")]
        public static double ActiveLevel { get; set; } = (int)LogLevel.FULLDEBUG;

        public static void Verbose(string text)
        {
            if (ActiveLevel < (int)LogLevel.VERBOSE) return;
            string level = "Verbose";
            WriteLog(text, level, LogLevel.VERBOSE);
        }

        public static void Error(string text)
        {
            if (ActiveLevel < (int)LogLevel.ERRORS) return;
            string level = "Error";
            WriteLog(text, level, LogLevel.ERRORS);
        }





        public static void Warning(string text)
        {
            if (ActiveLevel < (int)LogLevel.WARNINGS) return;
            string level = "Warning";
            WriteLog(text, level, LogLevel.WARNINGS);
        }
      

        public static void Debug(string text)
        {
            if (ActiveLevel < (int)LogLevel.FULLDEBUG) return;
            string level = "Debug";
            WriteLog(text, level, LogLevel.FULLDEBUG);
        }
        [ConsoleCommand("writelog,log")]
        public static String TestConsole(string[] data)
        {
            Verbose( String.Join(" ", data));

            return "";
        }
        private static void WriteLog(string text, string level, LogLevel lglevel)
        {
            Console.BackgroundColor = ConsoleColor.Black;


            switch (lglevel)
            {
                case LogLevel.NOTHING:
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case LogLevel.ERRORS:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case LogLevel.WARNINGS:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    break;
                case LogLevel.VERBOSE:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    break;
                case LogLevel.FULLDEBUG:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;
            }

            Directory.CreateDirectory("Logs");
            File.AppendAllText("Logs/" + DateTime.Now.ToString("ddMMyyyy") + ".log", "  [ " + level + " ]   " + DateTime.Now + " <<  " + text + "  >> |||\n");
            Console.WriteLine(DateTime.Now + " <<  " + text + "  >>\n");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

        }



    }
}

﻿using Newtonsoft.Json;

namespace UniBotSLX
{
    public static class Serealization
    {
            public static string SerializeToString(this object obj)
            {
                var JsonSerializer = JsonConvert.SerializeObject((obj), Formatting.Indented);

                return JsonSerializer;
            }

            public static T DeserializeString<T>(this string sourceString)
            {
               
                T JsonDesel = JsonConvert.DeserializeObject<T>(sourceString);
                return JsonDesel;
            }

        
    }
}

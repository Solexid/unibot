﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using VkBotClasses;
using VkNet.Model.RequestParams;

namespace UniBotSLX
{
    class PluginsSystem
    {
        const string PluginsDirectory = "Plugins";
        static List<Assembly> PluginAssemblies = new List<Assembly>();
        static List<MethodInfo> BotCommands=new List<MethodInfo>();
        public static void LoadPlugins() {
            Directory.CreateDirectory(PluginsDirectory);
           var files= Directory.GetFiles(PluginsDirectory, "*Plugin.dll");
            foreach (var item in files)
            {
                Logging.Verbose(item);
                PluginAssemblies.Add(Assembly.LoadFrom(item));
            }
            foreach (var assembly in PluginAssemblies)
            {
                Logging.Verbose(assembly.GetName().FullName);
                BotCommands.AddRange(assembly
                          .GetTypes()
                          .SelectMany(t => t.GetMethods())
                          .Where(m => (m.GetCustomAttributes(typeof(BotCmd), false)
                          .Any())));
                ConsoleSystemSLX.ConsoleSystem.AddAssembly(assembly);
            }

            BotCommands.AddRange(Assembly
                .GetExecutingAssembly()
                         .GetTypes()
                         .SelectMany(t => t.GetMethods())
                         .Where(m => (m.GetCustomAttributes(typeof(BotCmd), false)
                         .Any())));



            Logging.Debug("Plugins loaded.");


        }
        [BotCmd("список,комманды","Список всех комманд.\n Генерируется автоматически.")]
        public static void ListBotCmd(VkBotRequest request)
        {
            long ChatId = request.ChatID;
            string Message = request.Message;
            long MessageID = request.MessageID;
            long UserId = request.UserID;
            bool isChat = request.isChat;
            VkNet.Model.Message[] Messages = request.Forwarded;

            var VkMessage = new MessagesSendParams();
            if (isChat)
                VkMessage.PeerId = request.ChatID;
            else
                VkMessage.PeerId = request.UserID;
            Random Genereator = new Random();

            var result = "";

            try
            {



                if (BotCommands.Any())
                {
                    foreach (var item in BotCommands)
                    {
                        var obj = (((BotCmd)item.GetCustomAttributes(typeof(BotCmd), false)[0]));
             
                        result += "\n\n";
                        result += "\n "+obj.info +" - "+ obj.names;
                    }


                }
                else
                {
                    result = "Нет комманд!";
                }
            }
            catch (Exception e) { result = "Error = " + e.ToString(); }
            VkMessage.Message = result;


            request.ReceivedContextApi.Messages.Send(VkMessage);

        }


        public static bool InvokePlugin(string cmd,VkBotRequest request) {
            var result = false;
            request.cmd = cmd;
            var method = BotCommands.Where(m => (((BotCmd)m.GetCustomAttributes(typeof(BotCmd), false)[0]).Equals(cmd)) );
            if (method.Any()) {
                method.First().Invoke(null, new object[] { request });
                result = true;
            }
            return result;
        }


    }
}

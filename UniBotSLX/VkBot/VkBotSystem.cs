﻿using NetworkHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using UniBotSLX.EventSystem;
using VkBotClasses;
using VkNet;
using VkNet.Enums;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace UniBotSLX
{
   

    class VkUniBot
    {
        const string json_settings = "vk_bots.json";

       
        public static List<VkUniBot> VkBots = new List<VkUniBot>();
        private VkBotSettings settings { get; set; }
        private Thread WorkingThread ;
        private List<VkChat> Chats = new List<VkChat>();
        private ulong pts = 0;
        private ulong ts = 0;
        private Random _random = new Random();
        private ulong timer = 0;
        public ulong CommandsProcessed=0;
        private VkApi _api = null;
        public  VkApi ApiVk { get { CommandsProcessed++; return _api; } set { _api = value; } }
        

        public static bool InitBots() {
            bool result = false;
            VkBotSettings[] sets;
            PluginsSystem.LoadPlugins();
            EventSystem.EventSystem.BotMessage += OnMessagePlaceholder;
            try
            {
                sets = File.ReadAllText(json_settings).DeserializeString<VkBotSettings[]>();
            }
            catch (Exception e) { Logging.Error(" Error when reading "+json_settings+"!" + e.ToString());

                sets = new VkBotSettings[] { new VkBotSettings(), new VkBotSettings() };
                File.WriteAllText(json_settings, sets.SerializeToString());

            }
            foreach (var item in sets)
            {   if (item.Token != null && item.Enabled)
                { var bot = new VkUniBot(item);
                    VkBots.Add(bot);
                    bot.SetWorkingThread();
                    bot.Start();
                }
            }
            
            return result;
        

        }
        public static void OnMessagePlaceholder(object obj, MessageEvent e) {

            if (e.MsgType == BotMessageType.VK) {
                var context = ((VkUniBot)obj);
                var text = "";
               


                Logging.Debug(e.VkRequest.UserID + " " + e.VkRequest.Message);
                var cmd = e.MessageText.Split(' ').First().ToLower().Trim();
                if (e.VkRequest.BotSettings.CommandsBlackList.Contains(cmd)&&cmd.Length>1) {
                    if (e.VkRequest.isChat)
                    {
                        context.ApiVk.Messages.Send(new MessagesSendParams { PeerId = e.VkRequest.ChatID, Message = "Комманда в чёрном списке этого инстанса." });

                        context.ApiVk.Messages.MarkAsRead(null, e.VkRequest.ChatID.ToString());
                    }
                    else
                    {
                        context.ApiVk.Messages.Send(new MessagesSendParams { PeerId = e.VkRequest.UserID, Message = "Комманда в чёрном списке этого инстанса.." });

                        context.ApiVk.Messages.MarkAsRead(null, e.VkRequest.UserID.ToString());
                    }

 }else
                if (!PluginsSystem.InvokePlugin(cmd, e.VkRequest)) {
                if (e.VkRequest.isChat) {
                    if (context.settings.OwnerId == e.VkRequest.UserID) { text = ConsoleSystemSLX.ConsoleSystem.ParseFromConsole(e.MessageText); 
                    context.ApiVk.Messages.Send(new MessagesSendParams { PeerId = e.VkRequest.ChatID,Message=text + "." });
                    }
                    context.ApiVk.Messages.MarkAsRead(null, e.VkRequest.ChatID.ToString());
                }
                else {
                    if (context.settings.OwnerId == e.VkRequest.UserID)
                    {
                        text = ConsoleSystemSLX.ConsoleSystem.ParseFromConsole(e.MessageText);
                        context.ApiVk.Messages.Send(new MessagesSendParams { PeerId = e.VkRequest.UserID, Message = text+"." });
                    }
                    context.ApiVk.Messages.MarkAsRead(null, e.VkRequest.UserID.ToString());
                    }
                }
            }



        }

        public VkUniBot(VkBotSettings bot)
        {
            settings = bot;
            var logz = NLog.LogManager.GetLogger("vk_api");
           logz.IsEnabled(NLog.LogLevel.Off);
            ApiVk = new VkApi(logz, null,null);
            ApiVk.Authorize(new ApiAuthParams { AccessToken = bot.Token });
            Logging.Debug(ApiVk.IsAuthorized? "Authorized - " + bot.FriendlyBotName : "Not Authorized - " + bot.FriendlyBotName);
            if (ApiVk.IsAuthorized)
            {
                var user = ApiVk.Users.Get(new long[] { })[0];
                Logging.Debug(user.FirstName + " " + user.LastName);

            }
            else
                Logging.Error("User " + settings.BotNick + " not authorized");
        }

        public  void  SetWorkingThread() {

            List<long> AllMessages = new List<long>();
            MessagesGetLongPollHistoryParams LongPollParams = new MessagesGetLongPollHistoryParams();
            LongPollParams.Fields = new UsersFields { };
            LongPollParams.MsgsLimit = 201;
            

         WorkingThread = new Thread(delegate ()
           { 
            while (true)
               {
                  
                
                   if (pts == 0 || timer > 240)
                   {
                       ReloadChats();
                       var LongPollServer = ApiVk.Messages.GetLongPollServer(true, 2);
                       pts = (ulong)LongPollServer.Pts;
                       ts = LongPollServer.Ts;
                       timer = 0;

                   }
                   timer++;

                   LongPollParams.Pts = pts;
                   LongPollParams.Ts = ts;
                   if (!settings.SilentRequestsOnly) { 
                   AllMessages.Clear();
                   var VkMessages = ApiVk.Messages.GetLongPollHistory(LongPollParams);
                   pts = VkMessages.NewPts;
               
                   foreach (var VkMessage in VkMessages.Messages)
                   {
                       if (VkMessage.ReadState != MessageReadState.Readed)
                       {
                           var cmd = settings.BotNick.Split(',').Where(c => VkMessage.Text.ToLower().StartsWith(c.ToLower()));
                           if (cmd.Any())
                           {
                                   VkBotRequest RequestParams = new VkBotRequest();
                                   RequestParams.ReceivedContextApi = ApiVk;
                                   var FreeBot = GetFreeBot();
                                   RequestParams.ApiRequestContext = FreeBot.ApiVk;
                                   RequestParams.BotSettings = settings;
                                   RequestParams.Chats = Chats;
                                   RequestParams.Message = HttpUtility.UrlDecode(HttpUtility.UrlDecode(VkMessage.Text)).Replace("theync", "thеуnс").Replace("ync", "уnс").Remove(0, cmd.First().Length).Trim();
                                   RequestParams.MessageID = (long)VkMessage.Id;
                                   RequestParams.UserID = (long)VkMessage.FromId;
                                   RequestParams.Attachments = VkMessage.Attachments?.ToArray();
                                   RequestParams.Forwarded = VkMessage.ForwardedMessages?.ToArray();
                                   if (VkMessage.PeerId != VkMessage.FromId)
                                   {
                                   RequestParams.ChatID =  (long)VkMessage.PeerId;
                                   RequestParams.isChat = true;

                               }
                               else
                               {
                                   RequestParams.ChatID = (long)VkMessage.FromId;
                                   RequestParams.isChat = false;
                               }
                               EventSystem.EventSystem.OnMessageReceived(this, new EventSystem.MessageEvent { MsgType = BotMessageType.VK, VkRequest = RequestParams, MessageText = VkMessage.Text.Remove(0, cmd.First().Length).Trim() });

                             
                           }
                        
                       }
                   }
                   if (timer % 10 == 0) {

                            var data= HighLevelHelpers.GetString("https://api.vk.com/method/execute?code=return%20API.notifications.get();&v=5.80&access_token="+settings.Token);
                            var FriendRequests = ApiVk.Friends.GetRequests(new FriendsGetRequestsParams{ Count = 100,NeedMutual=false,Suggested=false }) ;
                           if (FriendRequests.Count > 0)
                           {
                               var users = GetFreeBot().ApiVk.Users.Get(FriendRequests.Items);

                               foreach (var user in users)
                               {
                                   Logging.Verbose("Friend request: "+user.FirstName + " " + user.LastName+" "+user.Domain);
                                   Logging.Verbose(ApiVk.Friends.Add(user.Id).SerializeToString());
                               }

                           }
                       }
                       CommandsProcessed++;
                   }
               
                   Thread.Sleep(1000+_random.Next(settings.MinDelay,settings.MaxDelay));
               }

           });

        }
        public  void ReloadChats()
        {
            var paramz = new MessagesDialogsGetParams();
            paramz.Count = 10;
            paramz.PreviewLength = 1;
            Chats.Clear();
            var ChatList = ApiVk.Messages.GetDialogs(paramz).Messages.Where(c => c.UsersCount > 2);
            foreach (var item in ChatList)
            {
                var chat = new VkChat();
                chat.ChatID= (long)item.ChatId;
                chat.UserIds = item.ChatActive.ToArray();
                Chats.Add(chat);
            }
            Logging.Debug("Chats reloaded. Count: " + Chats.Count());
        }
        public static VkUniBot GetFreeBot() {
            var min = ulong.MaxValue;
            VkUniBot result =null;
            foreach (var item in VkBots)
            {
                if (item.CommandsProcessed < min) {

                    min = item.CommandsProcessed;
                    result = item;

                }


            }
            result.CommandsProcessed++;
            return result;

        }
        public void Start() {

            WorkingThread.Start();

        }


    }
}

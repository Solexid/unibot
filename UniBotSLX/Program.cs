﻿using System;
using System.Threading;
using ConsoleSystemSLX;
namespace UniBotSLX
{
    class Program
    {
        [ConsoleVariable("inject_test")]
        public static double inject_test { get; set; } = 1337;

        static void Main(string[] args)
        {
           
         ConsoleSystem.InitConsole();
            VkUniBot.InitBots();
            Console.WriteLine("UniBot Load Complete.");
        
            var mre = new ManualResetEvent(false);

              ThreadPool.QueueUserWorkItem((state) =>
            {
               
                while (true)
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                   
                   
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                        var text = Console.ReadLine();
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine(ConsoleSystem.ParseFromConsole(text));
                   


                }
            });
          mre.WaitOne();
        }
    }
}

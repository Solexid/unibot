﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace NetworkHelpers
{
    public class HighLevelHelpers
    {
        public static string PostString(string data, string url)
        {


            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "POST";

            // access req.Headers to get/set header values before calling GetResponse. 
            // req.CookieContainer allows you access cookies.
            req.ContentLength = data.Length;

            req.ContentType = "application/x-www-form-urlencoded";
            req.ReadWriteTimeout = 1000;
            req.SendChunked = true;
            Stream dataStream = req.GetRequestStream(); var bytearray = Encoding.Convert(Encoding.GetEncoding(1251), Encoding.UTF8, Encoding.GetEncoding(1251).GetBytes(data));
            dataStream.Write(bytearray, 0, bytearray.Count());
            dataStream.Close();
            var response = req.GetResponse();
            string webcontent;
            using (var strm = new StreamReader(response.GetResponseStream()))
            {
                webcontent = strm.ReadToEnd();
            }
            return webcontent;

        }
        public static string GetString(string url)
        {


            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "GET";
            // access req.Headers to get/set header values before calling GetResponse. 
            // req.CookieContainer allows you access cookies.
            req.ReadWriteTimeout = 1000;
            var response = req.GetResponse();
            string webcontent;
            using (var strm = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            {
                webcontent = strm.ReadToEnd();
            }
            return webcontent;

        }
        public static string SendEmailMessage(string server, string login, string password, string from, string to, string subject, string text, byte[] bitmap)
        {
            string result = "";



            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredential =
                new NetworkCredential(login, password);
            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress(from);

            smtpClient.Timeout = 10500;

            smtpClient.Host = server.Split(':')[0];
            smtpClient.Port = int.Parse(server.Split(':')[1]);
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = basicCredential;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            message.From = fromAddress;
            message.Subject = subject;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            //Set IsBodyHtml to true means you can send HTML email.
            message.IsBodyHtml = true;
            if (bitmap != null) try
                {



                    MemoryStream qr = new MemoryStream(bitmap);
                    LinkedResource footerImg = new LinkedResource(qr, "image/bitmap");
                    footerImg.ContentId = "qr";
                    AlternateView foot = AlternateView.CreateAlternateViewFromString(text + "<p> <img src=cid:qr /> </p>", null, "text/html");

                    foot.LinkedResources.Add(footerImg);

                    message.AlternateViews.Add(foot);

                }
                catch { }



            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Priority = MailPriority.High;
            message.Body = text;
            message.To.Add(to);
            smtpClient.Send(message);
            try
            {

            }
            catch (Exception ex)
            {
                //Error, could not send the message
                result = (ex.Message);
            }


            return result;


        }
    }
}

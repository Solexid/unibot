﻿
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;

    namespace ConsoleSystemSLX
    {
        public class ConsoleCommand : Attribute
        {
            private string v;
            public ConsoleCommand()
            {
                this.v = "";
            }
            public ConsoleCommand(string v)
            {
                this.v = v;
            }
            public override bool Equals(object obj)
            {
                if (obj is string)
                {
                    var split = v.Split(',');
                    return split.Contains(((string)obj).Trim());
                }
                else
                    return base.Equals(obj);
            }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), v);
        }

        public override String ToString()
        {


            return v;


        }
    }
    public class ConsoleVariable : Attribute
        {
            private string v;
            public ConsoleVariable()
            {
                this.v = "";
            }
            public ConsoleVariable(string v)
            {
                this.v = v;
            }
            public override bool Equals(object obj)
            {
                if (obj is string)
                {
                    var split = v.Split(',');
                    return split.Contains(((string)obj).Trim());
                }
                else
                    return base.Equals(obj);
            }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), v);
        }

        public override String ToString()
            {


                return v;


            }

        }


    }

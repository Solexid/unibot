﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ConsoleSystemSLX
{
   public class ConsoleSystem
    {
       


        public static List<MethodInfo> Commands { get; private set; } = new List<MethodInfo>();
        public static List<PropertyInfo> Variables { get; private set; } = new List<PropertyInfo>();
        [ConsoleVariable("test_string,test_var")]
        public static string test_cvar { get; set; } = "Its works!";

        [ConsoleVariable("test_double")]
        public static double test_double { get; set; } = 228.1488;

        [ConsoleVariable("test_bool")]
        public static bool test_bool { get; set; } = true;

        [ConsoleVariable("launch_date,launched")]
        public static string launch_date { get; set; } = DateTime.Now.ToString();

        [ConsoleVariable("broken,launched")]//its not suppose to be visible in list
        public static int broken_var { get; set; } = -1;



        public static void InitConsole()
        {


            FillVariables();
            LoadVars();
            FillMethods();


        }
        public static void InitModule(Assembly module)
        {
            Commands.AddRange(module.GetTypes()
               .SelectMany(t => t.GetMethods())
               .Where(m => (m.GetCustomAttributes(typeof(ConsoleCommand), false)
               .Any())));
            FillMethods();

            Variables.AddRange(module
             .GetTypes()
             .SelectMany(t => t.GetProperties())
             .Where(m => (m.GetCustomAttributes(typeof(ConsoleVariable), false)
             .Any()) && ((m.GetValue(null) is string) || (m.GetValue(null) is bool) || (m.GetValue(null) is double))));
         }


        public static void AddAssembly(Assembly assembly) {
            Commands.AddRange(assembly.GetTypes()
              .SelectMany(t => t.GetMethods())
              .Where(m => (m.GetCustomAttributes(typeof(ConsoleCommand), false)
              .Any())));

        }
        static void FillMethods()
        {
            Commands.AddRange(Assembly.GetEntryAssembly().GetTypes()
                .SelectMany(t => t.GetMethods())
                .Where(m => (m.GetCustomAttributes(typeof(ConsoleCommand), false)
                .Any())));

            Commands.AddRange(Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .SelectMany(t => t.GetMethods())
                .Where(m => (m.GetCustomAttributes(typeof(ConsoleCommand), false)
                .Any())));

        }
        static void FillVariables()
        {
            Variables.AddRange(Assembly
               .GetEntryAssembly()
               .GetTypes()
               .SelectMany(t => t.GetProperties())
               .Where(m => (m.GetCustomAttributes(typeof(ConsoleVariable), false)
               .Any()) && ((m.GetValue(null) is string) || (m.GetValue(null) is bool) || (m.GetValue(null) is double))));

            Variables.AddRange(Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .SelectMany(t => t.GetProperties())
                .Where(m => (m.GetCustomAttributes(typeof(ConsoleVariable), false)
                .Any()) && ((m.GetValue(null) is string) || (m.GetValue(null) is bool) || (m.GetValue(null) is double))));
           
        }
         static void SaveVars() {

            // try  {
            Dictionary<string, string> dict = new Dictionary<string, string>();

                  if (Variables.Any())
            {
                foreach (var item in Variables)
                {
                    var obj = (((ConsoleVariable)item.GetCustomAttributes(typeof(ConsoleVariable), false)[0]));
                    String oldvalue = item.GetValue(null).ToString();
                    String var_name = obj.ToString().Split(',')[0];
                    dict.Add(var_name, oldvalue);
                }


            }

            File.WriteAllText("vars.json", dict.SerializeToString());
      //  }
     //       catch (Exception e){ Console.WriteLine("Error then writing variable data."); }
        }
        static void LoadVars()
        {
            try {

                Dictionary<string, string> dict = File.ReadAllText("vars.json").DeserializeString< Dictionary<string, string>>();
                foreach (var item in dict)
                {
                    SetVariable(item.Key.Split(',')[0], item.Value);
                }

            }
            catch (Exception e) { Console.WriteLine("Error then reading variable data."); }
        }


        public static string ParseFromConsole(string text)
        {


            var args = text.Split(' ').ToList();
            var cmd = args.First().ToLower();
            args.RemoveAt(0);
            var result = "Not found any console command with name " + cmd;
            var method = Commands.Where(m =>
                                (((ConsoleCommand)m.GetCustomAttributes(typeof(ConsoleCommand), false)[0]).Equals(cmd))
                                );
            if (method.Any())
            {
                try
                {
                    result = (String)method.First().Invoke(null, new object[] { args.ToArray() });
                }
                catch (Exception e) { result = e.Message; }

            }

            return result;

        }
        [ConsoleCommand("test")]
        public static String TestConsole(string[] data)
        {
            var result = "You written = " + String.Join("+", data);

            return result;
        }

        [ConsoleCommand("help")]
        public static String ListCommands(string[] data)
        {
            var result = "";

            try
            {



                if (Commands.Any())
                {
                    foreach (var item in Commands)
                    {
                        var obj = (((ConsoleCommand)item.GetCustomAttributes(typeof(ConsoleCommand), false)[0]));
                        String var_name = obj.ToString();
                        result += "\n--------------\n";
                        result += "\nCommand aliases = " + var_name;
                    }


                }
                else
                {
                    result = "No commands  in project!";
                }
            }
            catch (Exception e) { result = "Error = " + e.ToString(); }
            return result;
        }
        [ConsoleCommand("list_vars")]
        public static String ListVariables(string[] data)
        {
            var result = "";

            try
            {



                if (Variables.Any())
                {
                    foreach (var item in Variables)
                    {
                        var obj = (((ConsoleVariable)item.GetCustomAttributes(typeof(ConsoleVariable), false)[0]));
                        String oldvalue = item.GetValue(null).ToString();
                        String var_name = obj.ToString();
                        result += "\n--------------\n";
                        result += "\nVariable aliases = " + var_name;

                        result += "\nVariable type = " + item.GetValue(null).GetType().ToString();
                        result += "\nVariable value = " + oldvalue;
                    }


                }
                else
                {
                    result = "No console variables in project!";
                }
            }
            catch (Exception e) { result = "Error = " + e.ToString(); }
            return result;
        }
        public static String SetVariable(string name, string cvalue)
        {

            var result = "Not found";
            result = SetVariable(new string[] { name, cvalue });
            return result;
        }
        [ConsoleCommand("set")]
        public static String SetVariable(string[] data)
        {
            var result = "Not found";
            try
            {
                var var_name = data[0].ToLower();
                data[0] = "";
                var var_value = String.Join(" ", data).Remove(0, 1);
                var value = Variables.Where(m =>
                                    (((ConsoleVariable)m.GetCustomAttributes(typeof(ConsoleVariable), false)[0]).Equals(var_name))
                                    );
                if (value.Any())
                {
                    object oldvalue = value.First().GetValue(null);
                    if (oldvalue is string)
                    {
                        value.First().SetValue(null, var_value);

                    }
                    else if (oldvalue is double)
                    {
                        double new_value = 0;
                        try
                        {
                            new_value = double.Parse(var_value);
                            value.First().SetValue(null, new_value);
                        }
                        catch
                        {

                            result = "You write not double value!";
                            return result;
                        }


                    }
                    else if (oldvalue is bool)
                    {

                        bool new_value = false;
                        try
                        {
                            new_value = bool.Parse(var_value);
                            value.First().SetValue(null, new_value);
                        }
                        catch
                        {

                            result = "You write not bool value!";
                            return result;
                        }



                    }

                    result = "Value set." +
                        "\nOld value = " + oldvalue
                        + "\nNew value = " + var_value;

                }
                else
                {
                    result = "Variable with name " + var_name + " not found";
                }
            }
            catch (Exception e) { result = "Error = " + e.ToString(); }
            SaveVars();
            return result;
        }

        public static String GetVariable(string name)
        {

            var result = "Not found";
            result = GetVariable(new string[] { name });
            return result;
        }
        [ConsoleCommand("get")]
        public static String GetVariable(string[] data)
        {
            var result = "";
            try
            {

                var var_name = data[0].ToLower();
                var value = Variables.Where(m =>
                                         (((ConsoleVariable)m.GetCustomAttributes(typeof(ConsoleVariable), false)[0]).Equals(var_name))
                                         );
                if (value.Any())
                {
                    String oldvalue = value.First().GetValue(null).ToString();


                    result = "Variable value = " + oldvalue;

                }
                else
                {
                    result = "Variable with name " + var_name + " not found";
                }
            }
            catch (Exception e) { result = "Error = " + e.ToString(); }
            return result;
        }

    }

    public static class Serealization
    {
        public static string SerializeToString(this object obj)
        {
            var JsonSerializer = JsonConvert.SerializeObject((obj), Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize
            });

            return JsonSerializer;
        }

        public static T DeserializeString<T>(this string sourceString)
        {

            T JsonDesel = JsonConvert.DeserializeObject<T>(sourceString, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize
            });
            return JsonDesel;
        }


    }

}
